import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        events: [],
    },
    getters:{
        EVENTS: state => state.events
    },
    mutations:{
        ADD_EVENT: (state, event) => {
            state.events.push(event)
        },
        UPDATE_EVENT: (state, event) => {
            let index = state.events.findIndex( _event => _event.id == event.id)
            state.events[index].title = event.title;
            state.events[index].start = event.start;
            state.events[index].end = event.end;

        }
    },
    actions:{}
})

export default store;